#  OpeningHoursFragment

#### 项目介绍
- 项目名称：OpeningHoursFragment
- 所属系列：openharmony的第三方组件适配移植
- 功能：这是一个可重复使用的 UI 元素，用于编辑涵盖完整规范的开放时间值，适用于非破坏性编辑
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release  0.10.1
  
#### 效果演示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0729/151604_d4eb3fcd_8946212.gif "screen1.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0729/151622_d8d8b563_8946212.gif "screen2.gif")
  
  
#### 安装教程
  
1.在项目根目录下的build.gradle文件中，
  
 ```

allprojects {

    repositories {

        maven {

            url 'https://s01.oss.sonatype.org/content/repositories/releases/'

        }

    }

}

 ```
  
2.在entry模块的build.gradle文件中，

 ```

 dependencies {
    implementation('com.gitee.chinasoft_ohos:OpeningHoursFragment:1.0.1')
    ......  

 }

 ```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
  
#### 使用说明
  
1.将仓库导入到本地仓库中

2.在代码中跳转到OpeningHoursFragment,代码实例如下:
```
       ValueWithDescription key = new ValueWithDescription("collection_times", "Collection times");
              OpeningHoursFragment openingHoursFragment = OpeningHoursFragment.newInstance(key, null, 0, 5, true, textValues);
              Intent openingHoursFragmentIntent = openingHoursFragment.getIntent();
              openingHoursFragment.setOnSaveListener(this);
              presentForResult(openingHoursFragment, openingHoursFragmentIntent, 1000);
```
  
#### 测试信息
  
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异
  
  
#### 版本迭代
- 1.0.1
- 1.0.0
- 0.0.3-SNAPSHOT

#### 版权和许可信息
MIT
  