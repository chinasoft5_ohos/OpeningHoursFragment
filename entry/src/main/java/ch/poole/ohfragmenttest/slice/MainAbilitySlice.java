/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package ch.poole.ohfragmenttest.slice;

import ch.poole.ohfragmenttest.MainAbility;
import ch.poole.ohfragmenttest.ResourceTable;
import ch.poole.openinghoursfragment.OnSaveListener;
import ch.poole.openinghoursfragment.OpeningHoursFragment;
import ch.poole.openinghoursfragment.ValueWithDescription;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.ConfirmPopupView;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * MainAbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice implements OnSaveListener {
    private ArrayList<ValueWithDescription> textValues;
    private boolean hasSave;

    @Override
    public void onStart(Intent intent) {
        try {
            int color = getResourceManager().getElement(ResourceTable.Color_black).getColor();
            getWindow().setStatusBarColor(color);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        textValues = new ArrayList<>();
        ValueWithDescription yes = new ValueWithDescription("yes", "Yes");
        textValues.add(yes);
        ValueWithDescription no = new ValueWithDescription("no", "No");
        textValues.add(no);
        Logger.getLogger("TAG").log(Level.INFO, "ZOU L ZOU L ");
        ValueWithDescription key = new ValueWithDescription("collection_times", "Collection times");
        OpeningHoursFragment openingHoursFragment = OpeningHoursFragment.newInstance(key, null, 0, 5, true, textValues);
        Intent openingHoursFragmentIntent = openingHoursFragment.getIntent();
        openingHoursFragment.setOnSaveListener(this);
        presentForResult(openingHoursFragment, openingHoursFragmentIntent, 1000);
    }

    @Override
    protected void onActive() {
        super.onActive();
        if (hasSave) {
            final ConfirmPopupView popupView = new XPopup.Builder(getContext()).asConfirm("", "test fragment mode", "", "确定", new OnConfirmListener() {
                @Override
                public void onConfirm() {
                    Logger.getLogger("TAG").log(Level.INFO, "onConfirm");
                }
            }, null, false, ResourceTable.Layout_dialog_view);
            popupView.show();
            hasSave=false;
        }
    }

    @Override
    protected void onResult(int requestCode, Intent resultIntent) {
        super.onResult(requestCode, resultIntent);
        if (requestCode == 1000) {
            Logger.getLogger("TAG").log(Level.INFO, "哈哈哈哈");
        }
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void save(String key, String value) {
        hasSave = true;
        ValueWithDescription valueWithDescription = new ValueWithDescription("fee", null);
        OpeningHoursFragment openingHoursFragment = OpeningHoursFragment.newInstance(valueWithDescription, value, 0, 5, false, textValues);
        MainAbility ability = (MainAbility) getAbility();
        openingHoursFragment.setOnSaveListener(ability);
        Intent openingHoursFragmentIntent = openingHoursFragment.getIntent();
        presentForResult(openingHoursFragment, openingHoursFragmentIntent, 1000);
    }
}
