/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package ch.poole.ohfragmenttest;

import ch.poole.ohfragmenttest.slice.MainAbilitySlice;
import ch.poole.openinghoursfragment.OnSaveListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilityPackage;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.content.Intent;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.bundle.AbilityInfo;
import ohos.bundle.ElementName;
import ohos.bundle.IBundleManager;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * MainAbility
 */
public class MainAbility extends Ability implements OnSaveListener {
    private String[] permissions = new String[]{"ohos.permission.WRITE_USER_STORAGE", "ohos.permission.READ_USER_STORAGE"};

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        setAbilitySliceAnimator(null);
        Window window = getWindow();
        window.setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
    }

    @Override
    protected void onActive() {
        super.onActive();
        requestPermissions();
    }

    private void requestPermissions() {
        for (String permission : permissions) {
            if (verifySelfPermission(permission) != IBundleManager.PERMISSION_GRANTED) {
                requestPermissionsFromUser(
                        permissions, 1001);
            } else {
                // 权限已被授予
                Logger.getLogger("TAG").log(Level.INFO, "已授权");
            }
        }
    }

    @Override
    public void save(String key, String value) {
        Logger.getLogger("TAG").log(Level.INFO, key);
    }
}
