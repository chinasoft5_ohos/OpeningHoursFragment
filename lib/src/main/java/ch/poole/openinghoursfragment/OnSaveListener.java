package ch.poole.openinghoursfragment;

/**
 * Interface for listeners to return the constructed openinghous value
 * 
 * @author simon
 *
 */
public interface OnSaveListener {
    /**
     * save
     *
     * @param key key
     * @param value value
     */
    public void save(String key, String value);
}
