package ch.poole.openinghoursfragment;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

public class ValueWithDescription implements Sequenceable {
    private static final long serialVersionUID = 1L;
    private final String      value;
    private final String      description;

    /**
     * Construct a new instance
     * 
     * @param value the value
     * @param description the description if any
     */
    public ValueWithDescription(@NotNull final String value, @Nullable final String description) {
        this.value = value;
        this.description = description;
    }

    /**
     * String
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * String
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * toString
     * @return String
     */
    @Override
    public String toString() {
        return getDescription() != null ? getDescription() : getValue();
    }

    /**
     * hashCode
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return value == null ? 0 : value.hashCode();
    }

    /**
     * equals
     *
     * @param obj obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ValueWithDescription other = (ValueWithDescription) obj;
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }

    /**
     * marshalling
     *
     * @param parcel parcel
     * @return boolean
     */
    @Override
    public boolean marshalling(Parcel parcel) {
        return false;
    }

    /**
     * unmarshalling
     *
     * @param parcel parcel
     * @return boolean
     */
    @Override
    public boolean unmarshalling(Parcel parcel) {
        return false;
    }
}
