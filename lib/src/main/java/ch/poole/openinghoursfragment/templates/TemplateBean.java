package ch.poole.openinghoursfragment.templates;

/**
 * TemplateBean
 */
public class TemplateBean {
    /**
     * TemplateBean
     *
     * @param id id
     * @param isDefault isDefault
     * @param nameValue nameValue
     * @param keyValue keyValue
     * @param regionValue regionValue
     * @param objectValue objectValue
     * @param template template
     */
    public TemplateBean(int id, boolean isDefault, String nameValue, String keyValue, String regionValue, String objectValue,String template) {
        this.id = id;
        this.isDefault = isDefault;
        this.nameValue = nameValue;
        this.keyValue = keyValue;
        this.regionValue = regionValue;
        this.objectValue = objectValue;
        this.template=template;
    }

    private int id;
    private boolean isDefault;
    private String nameValue;
    private  String keyValue;
    private  String regionValue;
    private  String objectValue;
    private  String template;

    /**
     * getId
     *
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     * setId
     *
     * @param id id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * isDefault
     *
     * @return boolean
     */
    public boolean isDefault() {
        return isDefault;
    }

    /**
     * setDefault
     *
     * @param aDefault aDefault
     */
    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    /**
     * getNameValue
     *
     * @return String
     */
    public String getNameValue() {
        return nameValue;
    }

    /**
     * setNameValue
     *
     * @param nameValue nameValue
     */
    public void setNameValue(String nameValue) {
        this.nameValue = nameValue;
    }

    /**
     * getKeyValue
     *
     * @return String
     */
    public String getKeyValue() {
        return keyValue;
    }

    /**
     * setKeyValue
     *
     * @param keyValue keyValue
     */
    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    /**
     * getRegionValue
     *
     * @return String
     */
    public String getRegionValue() {
        return regionValue;
    }

    /**
     * setRegionValue
     *
     * @param regionValue regionValue
     */
    public void setRegionValue(String regionValue) {
        this.regionValue = regionValue;
    }

    /**
     * getObjectValue
     *
     * @return String
     */
    public String getObjectValue() {
        return objectValue;
    }

    /**
     * setObjectValue
     *
     * @param objectValue objectValue
     */
    public void setObjectValue(String objectValue) {
        this.objectValue = objectValue;
    }

    /**
     * getTemplate
     *
     * @return String
     */
    public String getTemplate() {
        return template;
    }

    /**
     * setTemplate
     *
     * @param template template
     */
    public void setTemplate(String template) {
        this.template = template;
    }
}
