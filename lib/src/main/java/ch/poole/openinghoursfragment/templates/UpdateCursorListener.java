package ch.poole.openinghoursfragment.templates;

import ohos.data.rdb.RdbStore;
import org.jetbrains.annotations.NotNull;

public interface UpdateCursorListener {

    /**
     * Replace the current cursor for the template database
     * 
     * @param db the template database
     */
    public void newCursor(@NotNull final RdbStore db);
}
