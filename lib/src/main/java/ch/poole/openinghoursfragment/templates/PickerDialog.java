package ch.poole.openinghoursfragment.templates;

import ch.poole.openinghoursfragment.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

/**
 * picker.dialog(IPickerDialog接口)自定义Dialog。提供DefaultPickerDialog，支持全局设定
 */
public class PickerDialog extends CommonDialog implements Component.ClickedListener {
    /**
     * Canceled dialog OnTouch Outside
     */
    public static final boolean S_DEFAULT_CANCELED_ON_TOUCH_OUTSIDE = true;
    private final Context mContext;

    private Text mBtnCancel, mBtnConfirm, mTvTitle;
    private OnDialogHideListener onDialogHideListener;
    private DirectionalLayout mRootLayout;

    /**
     * PickerDialog
     * @param context context
     *
     */
    public PickerDialog(Context context) {
        super(context);
        this.mContext = context;
        mRootLayout = (DirectionalLayout) LayoutScatter.getInstance(mContext)
                .parse(ResourceTable.Layout_template_dialog_ohos, null, false);
        mBtnCancel = (Text) mRootLayout.findComponentById(ResourceTable.Id_no);
        mBtnConfirm = (Text) mRootLayout.findComponentById(ResourceTable.Id_yes);
        mTvTitle = (Text) mRootLayout.findComponentById(ResourceTable.Id_title);
        mBtnCancel.setClickedListener(this);
        mBtnConfirm.setClickedListener(this);
    }



    public interface OnDialogHideListener {
        void onHide();
    }

    public void setOnDialogHideListener(OnDialogHideListener onDialogHideListener) {
        this.onDialogHideListener = onDialogHideListener;
    }

    /**
     * 先于onCreate(Bundle savedInstanceState)执行
     *
     */
    @Override
    public void onCreate() {
        setContentCustomComponent(mRootLayout);
        setAutoClosable(S_DEFAULT_CANCELED_ON_TOUCH_OUTSIDE);
        setTransparent(true);
    }

    @Override
    protected void onHide() {
        super.onHide();
        if (onDialogHideListener != null) {
            onDialogHideListener.onHide();
        }
    }


    @Override
    protected void onShowing() {
        super.onShowing();
    }

    @Override
    public void onClick(Component v) {
    }

    public Component getBtnCancel() {
        return mBtnCancel;
    }

    public Component getBtnConfirm() {
        return mBtnConfirm;
    }

    public Text getTitleView() {
        return mTvTitle;
    }

}
