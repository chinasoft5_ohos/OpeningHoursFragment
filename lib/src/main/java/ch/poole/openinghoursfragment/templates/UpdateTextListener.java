package ch.poole.openinghoursfragment.templates;


import org.jetbrains.annotations.NotNull;

public interface UpdateTextListener {

    /**
     * Update the OH string with a new text
     * 
     * @param newText the new text
     */
    public void updateText(@NotNull String newText);
}
