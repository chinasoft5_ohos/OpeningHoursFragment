package ch.poole.openinghoursfragment.templates;


import ch.poole.openinghoursfragment.ResourceTable;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;

public class TemplateDatabaseHelper {
    private static final String DEBUG_TAG = "TemplateDatabase";
    public static final String DATABASE_NAME = "openinghours_templates";
    private static final int DATABASE_VERSION = 4;
    private static Context context;
    private final StoreConfig storeConfig;
    private final DatabaseHelper databaseHelper;
    private RdbStore db;
    /**
     * TemplateDatabaseHelper
     *
     * @param context context
     */
    public TemplateDatabaseHelper(final Context context) {
        setApplicationContextStatic(context);
        databaseHelper = new DatabaseHelper(context.getApplicationContext());
        storeConfig = StoreConfig.newDefaultConfig(DATABASE_NAME);
        getDb();
    }

    /**
     * setApplicationContextStatic
     *
     * @param context context
     */
    private static void setApplicationContextStatic(final Context context) {
        TemplateDatabaseHelper.context = context;
    }

    private static final RdbOpenCallback rdbOpenCallback = new RdbOpenCallback() {
        @Override
        public void onCreate(RdbStore db) {

            db.executeSql("CREATE TABLE templates (key TEXT DEFAULT NULL, name TEXT, is_default INTEGER DEFAULT 0, template TEXT DEFAULT '', region TEXT DEFAULT NULL, object TEXT DEFAULT NULL)");
            TemplateDatabase.add(db, null, context.getString(ResourceTable.String_weekdays_with_lunch), true, "Mo-Fr 09:00-12:00,13:30-18:30;Sa 09:00-17:00;PH closed",
                    null, null);
            TemplateDatabase.add(db, null, context.getString(ResourceTable.String_weekdays_with_lunch_late_shopping), false,
                    "Mo,Tu,Th,Fr 09:00-12:00,13:30-18:30;We 09:00-12:00,13:30-20:00;Sa 09:00-17:00;PH closed", null, null);
            TemplateDatabase.add(db, null, context.getString(ResourceTable.String_weekdays), false, "Mo-Fr 09:00-18:30;Sa 09:00-17:00;PH closed", null, null);
            TemplateDatabase.add(db, null, context.getString(ResourceTable.String_weekdays_late_shopping), false,
                    "Mo,Tu,Th,Fr 09:00-18:30;We 09:00-20:00;Sa 09:00-17:00;PH closed", null, null);
            TemplateDatabase.add(db, null, context.getString(ResourceTable.String_twentyfourseven), false, "24/7", null, null);
            TemplateDatabase.add(db, "collection_times", context.getString(ResourceTable.String_collection_times_weekdays), true, "Mo-Fr 09:00; Sa 07:00; PH closed", null,
                    null);
        }

        @Override
        public void onUpgrade(RdbStore db, int i, int i1) {
            if (i <= 1 && i1 >= 2) {
                db.executeSql("ALTER TABLE templates ADD COLUMN key TEXT DEFAULT NULL");
                TemplateDatabase.add(db, "collection_times", context.getString(ResourceTable.String_collection_times_weekdays), true, "Mo-Fr 09:00; Sa 07:00; PH closed", null,
                        null);
            }
            if (i <= 2 && i1 >= 3) {
                TemplateDatabase.add(db, null, context.getString(ResourceTable.String_weekdays_with_lunch_late_shopping), false,
                        "Mo,Tu,Th,Fr 09:00-12:00,13:30-18:30;We 09:00-12:00,13:30-20:00;Sa 09:00-17:00;PH closed", null, null);
                TemplateDatabase.add(db, null, context.getString(ResourceTable.String_weekdays_late_shopping), false,
                        "Mo,Tu,Th,Fr 09:00-18:30;We 09:00-20:00;Sa 09:00-17:00;PH closed", null, null);
                TemplateDatabase.add(db, null, context.getString(ResourceTable.String_twentyfourseven), false, "24/7", null, null);
            }
            if (i <= 3 && i1 >= 4) {
                db.executeSql("ALTER TABLE templates ADD COLUMN region TEXT DEFAULT NULL");
                db.executeSql("ALTER TABLE templates ADD COLUMN object TEXT DEFAULT NULL");
            }
            onCreate(db);
        }
    };

    /**
     * 获取数据库
     *
     * @return RdbStore
     */
    public RdbStore getDb() {
        if (db != null && db.isOpen())
            return db;
        return db = databaseHelper.getRdbStore(storeConfig, 1, rdbOpenCallback);
    }
}
