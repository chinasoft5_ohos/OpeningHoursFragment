/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.poole.openinghoursfragment.menu;

import ch.poole.openinghoursfragment.OnMenuItemClickListener;

import com.lxj.xpopup.util.TextUtils;

import ohos.agp.components.Component;

import ohos.app.Context;

import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * MenuItem
 */
public class MenuItem extends Menu {
    private MeunItemType type;
    private int titleRes;
    private String title;
    private OnMenuItemClickListener onMenuItemClickListener;
    private boolean enabled = true;

    /**
     * isEnabled
     *
     * @return boolean
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * 构造方法
     *
     * @param type       type
     * @param titleRes   titleRes
     * @param context    context
     * @param menuLayout menuLayout
     */
    public MenuItem(MeunItemType type, int titleRes, Context context, Component menuLayout) {
        super(context, menuLayout);
        this.type = type;
        this.titleRes = titleRes;
    }

    /**
     * 构造方法
     * @param type type
     * @param title title
     * @param context context
     * @param menuLayout menuLayout
     */
    public MenuItem(MeunItemType type, String title, Context context, Component menuLayout) {
        super(context, menuLayout);
        this.type = type;
        this.title = title;
    }

    /**
     * getType
     * @return MeunItemType
     */
    public MeunItemType getType() {
        return type;
    }

    /**
     * setType
     *
     * @param type type
     */
    public void setType(MeunItemType type) {
        this.type = type;
    }

    /**
     * int
     *
     * @return getTitleRes
     */
    public int getTitleRes() {
        return titleRes;
    }

    /**
     * setTitleRes
     *
     * @param titleRes titleRes
     */
    public void setTitleRes(int titleRes) {
        this.titleRes = titleRes;
    }

    /**
     * setOnMenuItemClickListener
     *
     * @param onMenuItemClickListener onMenuItemClickListener
     */
    public void setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
        this.onMenuItemClickListener = onMenuItemClickListener;
    }

    /**
     * getOnMenuItemClickListener
     *
     * @return OnMenuItemClickListener
     */
    public OnMenuItemClickListener getOnMenuItemClickListener() {
        return onMenuItemClickListener;
    }

    /**
     * setEnabled
     *
     * @param b b
     */
    public void setEnabled(boolean b) {
        this.enabled = b;
    }

    /**
     * getTitle
     *
     * @return String
     */
    public String getTitle() {
        if (TextUtils.isEmpty(title)) {
            try {
                return getContext().getResourceManager().getElement(titleRes).getString();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (WrongTypeException e) {
                e.printStackTrace();
            }
        } else {
            return title;
        }

        return null;
    }
}
