/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.poole.openinghoursfragment.menu;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.TextField;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;

import ohos.app.Context;

import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.util.ArrayList;

/**
 * Description:
 * Create by lxj, at 2018/12/7
 */
public class MenuUtils {
    /**
     * 获取屏幕宽度
     *
     * @param context 上下文
     * @return 屏幕宽度
     */
    public static int getWindowWidth(Context context) {
        return context.getResourceManager().getDeviceCapability().width * context.getResourceManager().getDeviceCapability().screenDensity / 160;
    }

    /**
     * 获取屏幕的高度，包含状态栏，导航栏
     *
     * @param context 上下文
     * @return 屏幕的高度，包含状态栏，导航栏
     */
    public static int getScreenHeight(Context context) {
        return context.getResourceManager().getDeviceCapability().height * context.getResourceManager().getDeviceCapability().screenDensity / 160;
    }
    /**
     * 获取屏幕高度，不包含状态栏的高度
     *
     * @param context 上下文
     * @return 屏幕高度，不包含状态栏的高度
     */
    public static float getDisplayHeightInPx(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        Point point = new Point();
        display.getSize(point);
        return point.getPointY();
    }

    /**
     * vp转px
     *
     * @param context 上下文
     * @param vpValue vp
     * @return px
     */
    public static int vp2px(Context context, float vpValue) {
        return (int) (vpValue * context.getResourceManager().getDeviceCapability().screenDensity / 160);
    }



    /**
     * Return the navigation bar's height.
     *
     * @param context context
     * @return the navigation bar's height
     */
    public static int getNavBarHeight(Context context) {
        return (int) (getScreenHeight(context)-(double)getDisplayHeightInPx(context));
    }

    /**
     * 给控件设置宽高
     *
     * @param target 控件
     * @param width  宽
     * @param height 高
     */
    public static void setWidthHeight(Component target, int width, int height) {
        if (width <= 0 && height <= 0) {
            return;
        }
        ComponentContainer.LayoutConfig params = target.getLayoutConfig();
        if (width > 0) {
            params.width = width;
        }
        if (height > 0) {
            params.height = height;
        }
        target.setLayoutConfig(params);
    }

    /**
     * applyPopupSize
     *
     * @param content content
     * @param maxWidth maxWidth
     * @param maxHeight maxHeight
     * @param popupWidth popupWidth
     * @param popupHeight popupHeight
     * @param afterApplySize afterApplySize
     */
    public static void applyPopupSize(final ComponentContainer content, final int maxWidth, final int maxHeight, final int popupWidth, final int popupHeight, final Runnable afterApplySize) {
        content.addDrawTask(new Component.DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                ComponentContainer.LayoutConfig params = content.getLayoutConfig();
                Component implView = content.getComponentAt(0);
                ComponentContainer.LayoutConfig implParams = implView.getLayoutConfig();
                // 假设默认Content宽是match，高是wrap
                int contentWidth = content.getWidth();
                // response impl view wrap_content params
                if (maxWidth > 0) {
                    // 指定了最大宽度，就限制最大宽度
                    params.width = Math.min(contentWidth, maxWidth);
                    if (popupWidth > 0) {
                        params.width = Math.min(popupWidth, maxWidth);
                        implParams.width = Math.min(popupWidth, maxWidth);
                    }
                } else if (popupWidth > 0) {
                    params.width = popupWidth;
                    implParams.width = popupWidth;
                }

                int contentHeight = content.getHeight();
                if (maxHeight > 0) {
                    params.height = Math.min(contentHeight, maxHeight);
                    if (popupHeight > 0) {
                        params.height = Math.min(popupHeight, maxHeight);
                        implParams.height = Math.min(popupHeight, maxHeight);
                    }
                } else if (popupHeight > 0) {
                    params.height = popupHeight;
                    implParams.height = popupHeight;
                }
                implView.setLayoutConfig(implParams);
                content.setLayoutConfig(params);

                if (afterApplySize != null) {
                    afterApplySize.run();
                }
            }
        });
    }

    /**
     * createBitmapDrawable
     *
     * @param width width
     * @param color color
     * @return PixelMapElement
     */
    public static PixelMapElement createBitmapDrawable(int width, int color) {
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(width, 20);
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        PixelMap bitmap = PixelMap.create(initializationOptions);
        Texture texture = new Texture(bitmap);
        Canvas canvas = new Canvas(texture);
        Paint paint = new Paint();
        paint.setColor(new Color(color));
        RectFloat rectFloat1 = new RectFloat();
        rectFloat1.left = 0;
        rectFloat1.top = 0;
        rectFloat1.right = bitmap.getImageInfo().size.width;
        rectFloat1.bottom = 4;
        canvas.drawRect(rectFloat1, paint);
        paint.setColor(Color.TRANSPARENT);
        RectFloat rectFloat2 = new RectFloat();
        rectFloat2.left = 0;
        rectFloat2.top = 4;
        rectFloat2.right = bitmap.getImageInfo().size.width;
        rectFloat2.bottom = 20;
        canvas.drawRect(rectFloat2, paint);
        return new PixelMapElement(bitmap);
    }

    /**
     * createSelector
     *
     * @param defaultDrawable defaultDrawable
     * @param focusDrawable focusDrawable
     * @return StateElement
     */
    public static StateElement createSelector(Element defaultDrawable, Element focusDrawable) {
        StateElement stateListDrawable = new StateElement();
        stateListDrawable.addState(new int[]{}, focusDrawable);
        stateListDrawable.addState(new int[]{}, defaultDrawable);
        return stateListDrawable;
    }

    /**
     * findAllEditText
     *
     * @param list list
     * @param group group
     */
    public static void findAllEditText(ArrayList<TextField> list, ComponentContainer group) {
        for (int i = 0; i < group.getChildCount(); i++) {
            Component component = group.getComponentAt(i);
            if (component instanceof TextField && component.getVisibility() == Component.VISIBLE) {
                list.add((TextField) component);
            } else if (component instanceof ComponentContainer) {
                findAllEditText(list, (ComponentContainer) component);
            }
        }
    }







    /**
     * 获取布局是否是从右到左的布局
     *
     * @param context 上下文
     * @return 布局是否是从右到左的布局
     */
    public static boolean isLayoutRtl(Context context) {
        return false;
    }

    /**
     * 获取圆角背景
     *
     * @param color  背景颜色
     * @param radius 四个角的圆角度数
     * @return 圆角背景
     */
    public static ShapeElement createDrawable(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.RECTANGLE);
        drawable.setRgbColor(new RgbColor(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }

    /**
     * 获取圆角背景
     *
     * @param color    背景颜色
     * @param tlRadius 左上角的圆角度数
     * @param trRadius 右上角的圆角度数
     * @param brRadius 左下角的圆角度数
     * @param blRadius 右下角的圆角度数
     * @return 圆角背景
     */
    public static ShapeElement createDrawable(int color, float tlRadius, float trRadius, float brRadius, float blRadius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.RECTANGLE);
        drawable.setRgbColor(new RgbColor(color));
        drawable.setCornerRadiiArray(new float[]{
                tlRadius, tlRadius,
                trRadius, trRadius,
                brRadius, brRadius,
                blRadius, blRadius});
        return drawable;
    }

}
