/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.poole.openinghoursfragment.menu;

import ohos.agp.components.Component;

import ohos.app.Context;

import java.util.ArrayList;

/**
 * Menu
 */
public class Menu implements Component.ClickedListener {
    private Context context;
    /**
     * 点击的三个点  menuLayout
     */
    private Component menuLayout;
    /**
     * MenuItem集合
     */
    private ArrayList<MenuItem> menuItems;
    /**
     * 用与改变pop的宽度
     * 0 表示 worbContent  1 表示matchParent
     * 默认 worbContent
     */
    private int popWidthType = 0;
    /**
     * 是否显示在控件的下方
     */
    public boolean isShowBootom = false;

    /**
     * isShowBootom
     *
     * @return boolean
     */
    public boolean isShowBootom() {
        return isShowBootom;
    }

    /**
     * setShowBootom
     *
     * @param showBootom showBootom
     */
    public void setShowBootom(boolean showBootom) {
        isShowBootom = showBootom;
    }

    /**
     * 构造方法
     *
     * @param context    context
     * @param menuLayout menuLayout
     */
    public Menu(Context context, Component menuLayout) {
        this.context = context;
        this.menuLayout = menuLayout;
        menuItems = new ArrayList<>();
        setOnClickListener();
    }

    /**
     * addTopNoClick 添加无法点击的条目
     *
     * @param string_timespan_menu title
     */
    public void addTopNoClick(int string_timespan_menu) {
        MenuItem menuItem = new MenuItem(MeunItemType.NO_SUB, string_timespan_menu, context, menuLayout);
        menuItem.setEnabled(false);
        menuItems.add(menuItem);
    }

    /**
     * add 添加条目
     *
     * @param type     type
     * @param titleRes title
     * @return MenuItem
     */
    public MenuItem add(MeunItemType type, int titleRes) {
        MenuItem menuItem = new MenuItem(type, titleRes, context, menuLayout);
        if (this instanceof MenuItem) {

            menuItems.add(menuItem);
        } else {

            if (menuItems.size() >= 1) {
                menuItems.add(menuItems.size() - 1, menuItem);
            } else {
                menuItems.add(menuItem);
            }
        }
        return menuItem;
    }

    /**
     * add  添加条目
     *
     * @param menuItem menuItem
     * @return MenuItem
     */
    public MenuItem add(MenuItem menuItem) {
        if (this instanceof MenuItem) {
            menuItems.add(menuItem);
        } else {
            if (menuItems.size() >= 1) {
                menuItems.add(menuItems.size() - 1, menuItem);
            } else {
                menuItems.add(menuItem);
            }
        }


        return menuItem;
    }

    /**
     * getPopWidthType
     *
     * @return int
     */
    public int getPopWidthType() {
        return popWidthType;
    }

    /**
     * setPopWidthType
     *
     * @param popWidthType popWidthType
     */
    public void setPopWidthType(int popWidthType) {
        this.popWidthType = popWidthType;
    }

    /**
     * setOnClickListener
     */
    public void setOnClickListener() {
        if (this instanceof MenuItem) {
            return;
        }

        menuLayout.setClickedListener(this);

    }

    /**
     * showPop
     */
    public void showPop() {
        if (menuItems.size() == 0) {
            return;
        }
        MenuPop menuPop = new MenuPop(context);
        menuPop.setMenuItem(menuItems);
        menuPop.setPopWidthType(popWidthType);
        menuPop.setShowBootom(isShowBootom);
        menuPop.show();
        menuPop.doAttach(menuLayout);
    }

    /**
     * size
     *
     * @return int
     */
    public int size() {
        return menuItems.size();
    }

    /**
     * getContext
     *
     * @return Context
     */
    public Context getContext() {
        return context;
    }

    /**
     * getItem
     *
     * @param position position
     * @return MenuItem
     */
    public MenuItem getItem(int position) {
        if (position > menuItems.size() - 1) {
            return null;
        }
        return menuItems.get(position);
    }

    /**
     * onClick
     *
     * @param component component
     */
    @Override
    public void onClick(Component component) {
        showPop();
    }


}
