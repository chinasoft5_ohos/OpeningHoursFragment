package ch.poole.openinghoursfragment.pickers;

import ch.poole.openinghoursfragment.ResourceTable;
import ch.poole.openinghoursfragment.pickers.dialogpicker.DialogPickerView;
import ch.poole.openinghoursfragment.pickers.dialogpicker.NumberPickerView;
import ch.poole.openinghoursparser.YearRange;

import com.lxj.xpopup.impl.ConfirmPopupView;
import com.lxj.xpopup.interfaces.OnConfirmListener;

import ohos.app.Context;

import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * Display a dialog allowing the user to set a numeric range
 */
public class RangePicker {
    public static final int NOTHING_SELECTED = Integer.MIN_VALUE;

    private static final String TITLE = "title";
    private static final String MIN = "min";
    private static final String MAX = "max";
    private static final String START_CURRENT = "startCurrent";
    private static final String END_CURRENT = "endCurrent";
    private static SetRangeListener listener;

    private static final String TAG = "fragment_rangepicker";
    private static ConfirmPopupView popupView;
    private static String[] startValues;
    private static String[] endValues;
    private static NumberPickerView npvStart;
    private static NumberPickerView npvEnd;
    private static NumberPickerView npvStartVarDate;
    private static String[] varStartDateEntries;
    private static String[] varEndDateEntries;
    private static NumberPickerView npvEndVarDate;


    /**
     * Show the range picker dialog
     *
     * @param context       context
     * @param title         title
     * @param min           min
     * @param max           max
     * @param startCurrent  startCurrent
     * @param endCurrent    endCurrent
     * @param rangeListener rangeListener
     * @throws IOException
     */
    public static void showDialog(Context context, int title, int min, int max, int startCurrent, int endCurrent, SetRangeListener rangeListener) {
        try {
            final String titleStr = context.getResourceManager().getElement(title).getString();
            newInstance(context, titleStr, min, max, startCurrent, endCurrent, rangeListener);
            if (popupView != null) {
                popupView.show();
            }
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a new instance of RangePicker
     *
     * @param context       context
     * @param title         resource id for the title to display
     * @param min           minimum range value
     * @param max           maximum range value
     * @param startCurrent  initial start value
     * @param endCurrent    initial end value
     * @param rangeListener rangeListener
     * @return an instance of RangePicker
     */
    private static RangePicker newInstance(Context context, String title, int min, int max, int startCurrent, int endCurrent, SetRangeListener rangeListener) {
        RangePicker f = new RangePicker();
        listener = rangeListener;

        startValues = new String[max - min + 1];
        for (int i = min; i <= max; i++) {
            startValues[i - min] = Integer.toString(i);
        }
        endValues = new String[max - min + 2];
        endValues[0] = "-";
        for (int i = min; i <= max; i++) {
            endValues[i - min + 1] = Integer.toString(i);
        }

        varStartDateEntries = new String[]{"Start"};
        varEndDateEntries = new String[]{"End"};
        popupView = DialogPickerView.getInstance(context, title, "OK", ResourceTable.Layout_dialog_range_picker, new OnConfirmListener() {

            @Override
            public void onConfirm() {
                listener.setRange(getStartValue(npvStart), getEndValue(npvEnd));
            }

            private int getStartValue(final NumberPickerView start) {
                int yearValue = start.getValue();
                int returnValue;
                returnValue = yearValue + min;
                return returnValue;
            }

            private int getEndValue(final NumberPickerView end) {
                int yearValue = end.getValue();
                int returnValue;
                if (yearValue == 0) {
                    returnValue = NOTHING_SELECTED;
                } else {
                    returnValue = yearValue + min - 1;
                }
                return returnValue;
            }
        }, null).build();
        initDialogView(min, max, startCurrent, endCurrent);

        return f;
    }

    private static void initDialogView(int min, int max, int startCurrent, int endCurrent) {
        if (popupView != null) {
            npvStart = (NumberPickerView) popupView.findComponentById(ResourceTable.Id_start);
            npvStart.setDisplayedData(startValues);
            npvStart.setMinValue(0);
            npvStart.setMaxValue(startValues.length - 1);
            npvStart.setValue(startCurrent != YearRange.UNDEFINED_YEAR ? getStartValue(startCurrent) : 0);

            npvStartVarDate = (NumberPickerView) popupView.findComponentById(ResourceTable.Id_startVarDate);
            npvStartVarDate.setMinValue(0);
            npvStartVarDate.setMaxValue(1);
            npvStartVarDate.setDisplayedData(varStartDateEntries);
            npvStartVarDate.setValue(0);

            npvEnd = (NumberPickerView) popupView.findComponentById(ResourceTable.Id_end);
            npvEnd.setDisplayedData(endValues);
            npvEnd.setMinValue(0);
            npvEnd.setMaxValue(endValues.length - 1);
            npvEnd.setValue(endCurrent != YearRange.UNDEFINED_YEAR ? getEndValue(endCurrent) : 0);

            npvEndVarDate = (NumberPickerView) popupView.findComponentById(ResourceTable.Id_endVarDate);
            npvEndVarDate.setMinValue(0);
            npvEndVarDate.setMaxValue(1);
            npvEndVarDate.setDisplayedData(varEndDateEntries);
            npvEndVarDate.setValue(0);

        }
    }

    private static int getStartValue(int year) {
        int Value = 0;
        if (startValues == null || startValues.length == 0) {
            Value = 0;
        } else {
            for (int i = 0; i < startValues.length; i++) {
                if (startValues[i].equals("" + year)) {
                    Value = i;
                    break;
                }
            }
        }
        return Value;
    }

    private static int getEndValue(int year) {
        int Value = 0;
        if (endValues == null || endValues.length == 0) {
            Value = 0;
        } else {
            for (int i = 0; i < endValues.length; i++) {
                if (endValues[i].equals("" + year)) {
                    Value = i;
                    break;
                }
            }
        }
        return Value;
    }
}
