package ch.poole.openinghoursfragment.pickers;

import ch.poole.openinghoursparser.Month;
import ch.poole.openinghoursparser.VarDate;
import ch.poole.openinghoursparser.WeekDay;

import org.jetbrains.annotations.Nullable;

/**
 * Interface for listeners that return date values from DateRangePicker
 *
 * @author simon
 */
public interface SetDateRangeListener {
    /**
     * Listener for values from DateRangePicker
     *
     * @param startYear    selected start year
     * @param startMonth   selected start month or null
     * @param startWeekday selected start weekday or null
     * @param startDay     selected start day of the month or occurrence if startWeekday isn't null
     * @param startVarDate startVarDate
     * @param endYear      selected end year
     * @param endMonth     selected end month or null
     * @param endWeekday   selected end weekday or null
     * @param endDay       selected end day of the month or occurrence if endWeekday isn't null
     * @param endVarDate   endVarDate
     */
    void setDateRange(int startYear, @Nullable Month startMonth, @Nullable WeekDay startWeekday, int startDay, VarDate startVarDate, int endYear,
                      @Nullable Month endMonth, @Nullable WeekDay endWeekday, int endDay, VarDate endVarDate);
}
