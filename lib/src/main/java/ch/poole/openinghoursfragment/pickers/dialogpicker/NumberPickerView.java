/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package ch.poole.openinghoursfragment.pickers.dialogpicker;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Picker;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

import ohos.app.Context;

/**
 * NumberPickerView
 */
public class NumberPickerView extends Picker {
    public NumberPickerView(Context context) {
        super(context);
        init(null);
    }

    /**
     * Constructor
     *
     * @param context Context
     * @param attrSet attrSet
     */
    public NumberPickerView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    /**
     * Constructor
     *
     * @param context   Context
     * @param attrSet   attrSet
     * @param styleName styleName
     */
    public NumberPickerView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    /**
     * setDisplayedData
     *
     * @param displayedData displayedData
     */
    @Override
    public void setDisplayedData(String[] displayedData) {
        super.setDisplayedData(displayedData);
        setMaxValue(displayedData.length);
        setMinValue(0);
    }

    private void init(AttrSet attrSet) {

        if (null == attrSet || !attrSet.getAttr("wheel_mode_enabled").isPresent()) {
            setWheelModeEnabled(true);
        }
        if (null == attrSet || !attrSet.getAttr("bottom_line_element").isPresent()) {
            final ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromArgbInt(0XFFF56313));
            setDisplayedLinesElements(shapeElement, shapeElement);
        }
        if (null == attrSet || !attrSet.getAttr("normal_text_color").isPresent()) {
            setNormalTextColor(Color.BLACK);
        }
        if (null == attrSet || !attrSet.getAttr("selected_text_color").isPresent()) {
            setSelectedTextColor(new Color(0XFFF56313));
        }
        if (null == attrSet || !attrSet.getAttr("selected_normal_text_margin_ratio").isPresent()) {
            setSelectedNormalTextMarginRatio(3);
        }
        if (null == attrSet || !attrSet.getAttr("element_padding").isPresent()) {
            setCompoundElementPadding(10);
        }


    }
}
