/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package ch.poole.openinghoursfragment.pickers.dialogpicker;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.ConfirmPopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;

import ohos.app.Context;

/**
 * DialogPickerView
 */
public class DialogPickerView {
    private static Context cont;
    private ConfirmPopupView popupView;
    private static String titleStr;
    private static String sure;
    private static int ResLayout;
    private static OnConfirmListener confirm;
    private static OnCancelListener cancel;

    /**
     * DialogPickerView构造方法
     */
    public DialogPickerView() {

    }

    private static DialogPickerView picker = new DialogPickerView();

    /**
     * getInstance
     *
     * @param context          context
     * @param title            title
     * @param sureText         sureText
     * @param res              res
     * @param confirmListener  confirmListener
     * @param onCancelListener onCancelListener
     * @return DialogPickerView
     */
    public static DialogPickerView getInstance(Context context, String title, String sureText, int res, OnConfirmListener confirmListener, OnCancelListener onCancelListener) {
        cont = context;
        titleStr = title;
        sure = sureText;
        ResLayout = res;
        confirm = confirmListener;
        cancel = onCancelListener;
        return picker;
    }

    /**
     * build
     *
     * @return ConfirmPopupView
     */
    public ConfirmPopupView build() {
        popupView = new XPopup.Builder(cont).asConfirm(titleStr, "", "CANCEL", sure, confirm, cancel, false, ResLayout);
        return popupView;
    }
}
