package ch.poole.openinghoursfragment;

import ch.poole.openinghoursfragment.menu.MenuItem;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;

public interface OnMenuItemClickListener {
    /**
     * Called when a menu item has been invoked.  This is the first code
     * that is executed; if it returns true, no other callbacks will be
     * executed.
     *
     * @param item The menu item that was invoked.
     *
     * @return Return true to consume this click and prevent others from
     *         executing.
     */
    public boolean onMenuItemClick(MenuItem item);
}
