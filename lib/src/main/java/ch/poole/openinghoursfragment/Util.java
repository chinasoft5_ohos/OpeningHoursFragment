package ch.poole.openinghoursfragment;


import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Rect;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import org.angmarch.views.NiceSpinner;
import org.angmarch.views.NiceSpinner;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public final class Util {
    protected static final String DEBUG_TAG = "Util";

    /**
     * Private default constructor
     */
    private Util() {
        // Empty
    }

    /**
     * Scroll to the supplied view
     * 
     * This code is from vespucci and licensed under the Apache 2.0 licence
     * 
     * @author simon
     * 
     * @param sv the ScrollView or NestedScrollView to scroll
     * @param row the row to display, if null scroll to top or bottom of sv
     * @param up if true scroll to top if row is null, otherwise scroll to bottom
     * @param force if true always try to scroll even if row is already on screen
     */
    public static void scrollToRow(@NotNull final ComponentContainer sv, @Nullable final Component row, final boolean up, boolean force) {
        Rect scrollBounds = new Rect();

        sv.getSelfVisibleRect(scrollBounds);
        //暂时找到的获取坐标的方法 是上面的方法 不知道可行不可行
        if (row != null && row.getSelfVisibleRect(scrollBounds) && !force) {
            return; // already on screen
        }
        if (row == null) {
            new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
                @Override
                public void run() {
                    if (sv instanceof ScrollView) {
                        if(up){
                            ((ScrollView) sv).scrollTo(0,0);
                        }else{
                            int childCount = sv.getChildCount();
                            Component componentAt = sv.getComponentAt(childCount - 1);
                            int i = componentAt.getBottom() + sv.getPaddingBottom();
                            ((ScrollView) sv).scrollTo(0,i);
                        }
                    } else if (sv instanceof NestedScrollView) {
                        if(up){
                            ((NestedScrollView) sv).scrollTo(0,0);
                        }else{
                            int childCount = sv.getChildCount();
                            Component componentAt = sv.getComponentAt(childCount - 1);
                            int i = componentAt.getBottom() + sv.getPaddingBottom();
                            ((NestedScrollView) sv).scrollTo(0,i);
                        }
                    } else {
                        LogUtil.error("scrollToRow unexpected view " + sv);
                    }
                }
            });


        } else {
            new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
                @Override
                public void run() {
                    final int target = up ? row.getTop() : row.getBottom();
                    if (sv instanceof ScrollView) {
                        ((ScrollView) sv).fluentScrollTo(0, target);
                    } else if (sv instanceof NestedScrollView) {
                        ((NestedScrollView) sv).fluentScrollTo(0, target);
                    } else {
                        LogUtil.error("scrollToRow unexpected view " + sv);
                    }
                }
            });

        }
    }

    /**
     * Display a toast at the top of the screen
     * 
     * @param context  context
     * @param msg the message to display
     */
    public static void toastTop(@NotNull Context context, String msg) {
        LayoutScatter inflater = LayoutScatter.getInstance(context);
        Component component = inflater.parse(ResourceTable.Layout_toast, null, false);

        Text text = (Text) component.findComponentById(ResourceTable.Id_text);
        text.setText(msg);
        ToastDialog toast = new ToastDialog(context);
        toast.setAlignment(LayoutAlignment.TOP);
        toast.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT,ComponentContainer.LayoutConfig.MATCH_CONTENT);
        toast.setDuration(1000);
        toast.setComponent(component);
        toast.show();
    }

    /**
     * Display a toast at the top of the screen
     * 
     * @param context  context
     * @param msg the message resource id to display
     */
    public static void toastTop(@NotNull Context context, int msg) {
        LayoutScatter inflater = LayoutScatter.getInstance(context);
        Component component = inflater.parse(ResourceTable.Layout_toast, null, false);

        Text text = (Text) component.findComponentById(ResourceTable.Id_text);
        text.setText(msg);
        ToastDialog toast = new ToastDialog(context);
        toast.setAlignment(LayoutAlignment.HORIZONTAL_CENTER);
        toast.setDuration(1000);
        toast.setComponent(component);
        toast.show();
    }

    /**
     * Set initially selected value on a spinner
     *
     * @param resourceManager the ResourceManager
     * @param valuesId the value array resource id
     * @param spinner the Spinner
     * @param value the value to set
     */
    public static void setSpinnerInitialEntryValue(@NotNull ResourceManager resourceManager, int valuesId, @NotNull NiceSpinner spinner, @Nullable String value) {
        try {
            String[] values = resourceManager.getElement(valuesId).getStringArray();
            for (int i = 0; i < values.length; i++) {
                if ((value == null && "".equals(values[i])) || (value != null && value.equals(values[i]))) {
                    spinner.setSelectedIndex(i);
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }

    }


    public static String[] getStrings(@NotNull ResourceManager resourceManager, int valuesId) {
        String[] stringArray = new String[0];
        try {
            stringArray = resourceManager.getElement(valuesId).getStringArray();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return stringArray;
    }
}
