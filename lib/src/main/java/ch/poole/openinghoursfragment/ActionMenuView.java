package ch.poole.openinghoursfragment;

import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

public class ActionMenuView extends DirectionalLayout {
    public ActionMenuView(Context context) {
        super(context);
    }

    public ActionMenuView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public ActionMenuView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}
