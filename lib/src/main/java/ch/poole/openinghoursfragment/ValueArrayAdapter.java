package ch.poole.openinghoursfragment;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

public class ValueArrayAdapter extends BaseItemProvider {

    final List<ValueWithDescription> values;
    private Context context;

    /**
     * Construct a new instance
     *
     * @param context  Context
     * @param values  a List of ValuesWithDescription
     */
    public ValueArrayAdapter(List<ValueWithDescription> values, Context context) {
        this.values = values;
        this.context = context;
    }

    @Override
    public int getCount() {
        return values == null ? 0 : values.size();
    }

    @Override
    public Object getItem(int i) {
        return values == null ? null : values.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        final Text view;
        if (component == null) {
            view = (Text) LayoutScatter.getInstance(context).parse(ResourceTable.Layout_simple_spinner_item, componentContainer, false);
        } else {
            view = (Text) component;
        }
        view.setText(values.get(i).getValue());

        return view;
    }
}
